/* Test blocking, raising SIGCHLD, default handling, and unblocking. */

#include "signal.h"

static void handler(int signum)
{
	(void) signum;
	printf("SIGCHLD\n");
}

int main(void)
{
	sigset_t sigchld;
	sigemptyset(&sigchld);
	sigaddset(&sigchld, SIGCHLD);
	sigprocmask(SIG_BLOCK, &sigchld, NULL);
	signal(SIGCHLD, handler);
	raise(SIGCHLD);
	signal(SIGCHLD, SIG_DFL);
	sigprocmask(SIG_UNBLOCK, &sigchld, NULL);
	return 0;
}

#!/bin/sh

if [ $# != 1 ]; then
  echo "usage: $0 PATH-TO-OS-TEST" >&2
  exit 1
fi

os_test="$1"
in="$os_test/out"

(cd $os_test && git rev-parse HEAD) > os-test.commit

rm -rf expectations
mkdir expectations
for suite in $(cat "$os_test/misc/suites.list"); do
  cp -R -- "$os_test/$suite.expect" "expectations/$suite"
done

rm -rf suites
mkdir suites
for suite in $(cat "$os_test/misc/suites.list"); do
  mkdir -p -- "suites/$suite"
  cp -- "$os_test/$suite/"README "suites/$os/$suite"
  cp -- "$os_test/$suite/"*.c "suites/$os/$suite"
  cp -- "$os_test/$suite/"*.h "suites/$os/$suite"
done

rm -rf outcomes
mkdir outcomes
os_list=$(ls -- "$in")
for os in $os_list; do
  mkdir -p -- "outcomes/$os"
  cp -- "$in/$os/uname.out" "outcomes/$os/uname.out"
  for suite in $(cat "$in/$os/misc/suites.list"); do
    mkdir -p -- "outcomes/$os/$suite"
    cp -- "$in/$os/$suite/"*.out "outcomes/$os/$suite"
  done
done
